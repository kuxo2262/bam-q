### General remarks

The intrusive binaural audio quality model BAM-Q (Fle�ner et al., 2017) requires the clean 
and the distorted signal as input to estimate perceived binaural quality differences.
Stereo signals ([left channel right channel]) are required as model input. A stimuli level of 
0 dB FS corresponds to a sound pressure level of 115 dB SPL.
Clean and distorted signals have to be the same length and temporal aligned. The input signals are required to
have a duration of at least 0.4 seconds.


'example_BAMQ.m' gives a minimal example how to use BAM-Q in Matlab. 

The BAM-Q output provides four submeasures:
- **binQ**:         ... binaural quality measure; based on a combination of of the submeasures that represent differences 
                    between the reference and the test signal for interaural level differences (ILDdiff), 
                    interaural time/phase differences (ITDdiff) and the interaural vector strength ('IVSdiff').
    - 100 ... no difference
    - 0   ... large difference
    - -X  ... even larger difference
-  **ILDdiff**      ... intermediate ILD measure
-  **ITDdiff      ... intermediate ITD measure (can be 0 if ITDs are not evaluable)
-  **IVSdiff**      ... intermediate IVS measure


A more detailed description of BAM-Q is given in:

J.-H. Fle�ner, R. Huber, and S. D. Ewert, "Assessment and Prediction of Binaural Aspects of Audio Quality",
Journal of the Audio Engineering Society, vol. 65, no.11, PP.929-942. 2017. https://doi.org/10.17743/jaes.2017.0037

**Abstract:**
Binaural or spatial presentation of audio signals has become increasingly important in
consumer sound reproduction, but also for hearing assistive devices like hearing aids, where
signals in both ears might undergo heavy signal processing. Such processing might introduce
distortions to the interaural signal properties that affect perception. Here, an approach for
intrusive binaural auditory-model-based quality prediction (BAM-Q) is introduced. BAM-Q
uses a binaural auditory model as front-end to extract the three binaural features interaural
level difference, interaural time difference, and a measure of interaural coherence. The current
approach focuses on the general applicability (with respect to binaural signal differences) of
the binaural quality model to arbitrary binaural audio signals. Thus, two listening experiments
were conducted to subjectively measure the influence of these binaural features and their
combinations on binaural quality perception. The results were used to train BAM-Q. Two
different hearing aid algorithms were used to evaluate the performance of the model. The
correlations between subjective mean ratings and model predictions are higher than 0.9.


Authors of the Matlab implementation of BAM-Q: 

- jan-hendrik.flessner@jade-hs.de
- thomas.biberger@uni-oldenburg.de (revision of the implementation of the feature combination within the model back end)

===============================================================================
### License and permissions
===============================================================================

Unless otherwise stated, the BAM-Q distribution, including all files is licensed
under Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
(CC BY-NC-ND 4.0).
In short, this means that you are free to use and share (copy, distribute and
transmit) the BAM-Q distribution under the following conditions:

Attribution - You must attribute the BAM-Q distribution by acknowledgement of
              the author if it appears or if was used in any form in your work.
              The attribution must not in any way that suggests that the author
              endorse you or your use of the work.

Noncommercial - You may not use BAM-Q for commercial purposes.
 
No Derivative Works - You may not alter, transform, or build upon BAM-Q.

Exceptions are the following external Matlab functions (see their respective licence)
that were used within the BAM-Q:
- Gammatone filterbank from V. Hohmann (https://zenodo.org/record/2643400#.XQsf5TnVLCM), for details see[1,2]:
   [1] Hohmann, V. (2002). Frequency analysis and synthesis using a Gammatone filterbank. 
       Acta Acustica united with Acustica, 88(3), 433-442.
   [2] Herzke, T., & Hohmann, V. (2007). Improved numerical methods for gammatone filterbank analysis and
       synthesis. Acta acustica united with acustica, 93(3), 498-500. 
- Code snipets from the Dietz Modell (Authors: Mathias Dietz, Martin-Klein Hennig), for details see:
      M. Dietz, S. D. Ewert, and V. Hohmann. Auditory model based direction estimation of concurrent speakers
      from binaural signals. Speech Communication, 53(5):592-605, 2011.
  