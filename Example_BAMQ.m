% EXAMPLE for the usage of the binaural audio quality model BAM-Q that is described
% in Fle�ner et al. (2017). It is a reference-based audio quality model and based on
% binaural cues as interaural level and phase differences (ILDs, IPDs) and interaural
% vector strength (IVS) to predict subjective audio quality ratings. 
% Model input: 
% - Stereo signals are required as input [left_ch right ch]
% - 0 dB FS should correspond to 115 dB SPL.
% - Clean and distorted signals have to be the same length and temporal aligned.
% - The input signals are required to have a duration of at least 0.4
%   seconds.
% Model output, binQ:
% 100 ... no difference
% 0   ... large difference
% -X  ... even larger difference
           

cur_dir=pwd;
addpath(genpath(cur_dir)); % include all folders from the current path
%% input signals

if verLessThan('matlab','8.0')
 % reference signal (clean)
[RefSig, fsRef] = wavread('Stimuli/+0.00_Referenzsignal.wav');
% test signal (processed)
[TestSig, fsTest] = wavread('Stimuli/+0.00_Referenzsignal.wav');
else
 % reference signal (clean)
[RefSig, fsRef] = audioread('Stimuli/+0.00_Referenzsignal.wav');
% test signal (processed)
[TestSig, fsTest] = audioread('Stimuli/+0.50_Referenzsignal.wav');
end

% compare sampling frequencies
if fsTest ~= fsRef,
    error('signals have different sampling frequencies')
else
    fs = fsTest;
end

[binQ, ILDdiff, ITDdiff, IVSdiff] = BAMQ4Public_restruct(RefSig, TestSig, fs);

disp('***********************')
disp('***binaural measures***')
disp('***********************')
disp(['binQ: ', num2str(binQ)])
disp(['ILDdiff: ', num2str(ILDdiff)])
disp(['ITDdiff: ', num2str(ITDdiff)])
disp(['IVSdiff: ', num2str(IVSdiff)])
%     binQ: 83
%     ILDdiff: 2.0702e+03
%     ITDdiff: 7.3006e-05
%     IVSdiff: 0.0111